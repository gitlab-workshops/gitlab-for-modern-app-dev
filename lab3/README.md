# Lab 3 - Make Some Code Changes

In the last lab we got setup to work and then opened the built-in Web IDE on our new branch. You can of course edit with any other git compatible tooling, but we'll keep it simple for today. Let's now go and make a small change to our project.

## Edit the Code
In the Web IDE, on the left side file tree, navigate down to `HelloController.java` and click on it to view and edit. (`src` -> `main` -> `java` -> `hello` -> `HelloController.java`)

![Navigate to file](images/create/webide_file_nav.png)

Make 4 small edits to this file:

1. Change the background color to `#6b4fbb` by replacing the highlighted text.
   ![File change background color](images/create/file_change_bg.png)

1. Edit the message and put whatever you want (suggestion: replace `Spring Boot` with `GitLab!!`.)
   ![File change message](images/create/file_change_message.png)

1. Add a graphic to the message body by pasting the following code after `line 16`:
   ```
   message += "<p><center><img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki' width='250'></center>";
   ```
   ![File change add to message](images/create/file_change_message_add.png)

1. Remove the `TODO` comment line (delete all of line 15)
![File change todo delete](images/create/file_change_todo_delete.png)

## Commit the Changes
When you are done editing, your file should look as pictured below. Click on the blue `Commit...`button (Hint: You might have to do this twice depending on the size of your browser window).

![Commit changes](images/create/webide_commit.png)

Add a short commit message and make sure the option of `Commit to <your branch name> branch` is selected, and then click on the green `Stage & Commit` button.

![Stage and commit changes](images/create/webide_commit_2.png)


## Congratulations!!

![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)

<p>
You have made several code changes, and on committing them, GitLab automatically picked up and ran your changes through the pipeline, including building them (remember you never defined how to do that), testing them, security scanning them, packaging them, deploying them, and monitoring them.

## Proceed to Lab 4 - Review Pipeline Results and Deploy to Production

Continue to [lab 4](../lab4/README.md)
